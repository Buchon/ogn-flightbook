import logging
import traceback
from datetime import datetime
from pytz import timezone as pytz_timezone, utc as pytz_utc

from flask import Flask, jsonify, request, abort, Response
from werkzeug.wsgi import FileWrapper
import redis

from core.live import address as laddress, path as lpath, fleet as lfleet, igc_ as igc, positions
from web.wlbook import WLogbook
from utils.db import Db
from utils.device import mapping_address, get_rdevice
import setting

app = Flask(__name__)
pool = redis.ConnectionPool(host=setting.REDIS_HOST, port=setting.REDIS_PORT, db=setting.REDIS_DB,
                            decode_responses=True, encoding="utf-8")
red = redis.Redis(connection_pool=pool)


@app.route('/api/logbook/<code>', defaults={'date': None}, strict_slashes=False)
@app.route('/api/logbook/<code>/<date>', strict_slashes=False)
def make_logbook(code, date=None):
    db = Db(path=setting.SQLITE_PATH)
    logbook = WLogbook(date=date, code=code.upper(), red=red, db=db)
    logbook.create()
    response = jsonify(logbook.to_json())
    response.headers.add('Access-Control-Allow-Origin', '*')
    db.close()
    return response


@app.route('/api/autocomp/<query>', strict_slashes=False)
def autocomplete(query):
    db = Db(path=setting.SQLITE_PATH)
    response = list()
    elts = db.search_airfield(query=query, limit=5)
    id_ = 0
    for elt in elts:
        response.append({"id": id_, "code": elt[0], "name": elt[1], "elevation": elt[2], "tz": elt[3]})
        id_ += 1

    response = jsonify(response)
    response.headers.add('Access-Control-Allow-Origin', '*')
    db.close()
    return response


@app.route('/api/airfield', defaults={'days': None}, strict_slashes=False, methods=['GET', 'POST'])
@app.route('/api/airfield/<int:days>', strict_slashes=False, methods=['GET', 'POST'])
def search_null(days):
    """
    helper for finding potential airfields unregister in system (OACI code null and aircraft stopped)
    :param days: int, nb of days. starting search from today - nb of days
    :return: json aircraft(s) array
    """
    db = Db(path=setting.SQLITE_PATH)
    gspnull = list()
    if request.method == "GET":
        addresses = db.search_null(minus_day=days, strict=False)
        gspnull = positions(red=red, addresses=addresses)
    elif request.method == "POST":
        addresses = db.search_null(minus_day=days, strict=True)
        gspnull = positions(red=red, addresses=addresses, strict=True)
    db.close()

    response = jsonify(gspnull)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/api/live/address/<address>')
def live_address(address):
    response = laddress(red=red, address_=address)
    response = jsonify(response)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/api/live/path/<address>', defaults={'start': None, 'stop': None}, strict_slashes=False)
@app.route('/api/live/path/<address>/<start>', defaults={'stop': None}, strict_slashes=False)
@app.route('/api/live/path/<address>/<start>/<stop>')
def live_path(address, start, stop):
    headers = request.headers
    auth = headers.get("X-Api-Key", None)

    if start is not None:
        try:
            start = int(start)
        except ValueError:
            start = None
    if stop is not None:
        try:
            stop = abs(int(stop))
        except ValueError:
            stop = None

    if auth is not None and auth == setting.X_API_KEY:
        response = jsonify(lpath(red=red, address_=address, start=start, stop=stop, privacy=False))
    else:
        response = jsonify(lpath(red=red, address_=address, start=start, stop=stop))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/api/live/igc/<address>', defaults={'start': None, 'stop': None}, strict_slashes=False)
@app.route('/api/live/igc/<address>/<start>', defaults={'stop': None}, strict_slashes=False)
@app.route('/api/live/igc/<address>/<start>/<stop>')
def live_igc(address, start, stop):
    address = address.upper()
    if start is not None:
        try:
            start = abs(int(start))
        except ValueError:
            start = None
    if stop is not None:
        try:
            stop = abs(int(stop))
        except ValueError:
            stop = None
    headers = request.headers
    auth = headers.get("X-Api-Key", None)
    # privacy concerns
    privacy = False if auth is not None and auth == setting.X_API_KEY else True
    map_address = mapping_address(address=address, red=red) if privacy else address
    args = request.args
    date_args = args.get('date', None)
    airfield_args = args.get("airfield", None)

    # try to fix the date in igc flight time zone
    date_ = None
    # date_args is generally given by webapp
    if date_args is not None:
        try:
            date_ = datetime.strptime(date_args, '%Y-%m-%d')
        except ValueError:
            logging.error('bad date_args: {}'.format(date_args))
    # airfields_args is generally given by igcd, need to inspect airfield tz in this case
    if airfield_args is not None:
        db = Db(path=setting.SQLITE_PATH)
        match_airfield = db.match_airfield(code=airfield_args)
        if match_airfield is not None:
            date_ = datetime.now(pytz_timezone(match_airfield[2]))
    # finally set date (utc) if not already define
    if date_ is None:
        date_ = datetime.now(pytz_utc)

    aircraft = registration = competition = None
    # address starting with ~ or _ will not be resolve
    if address[0] not in ['~', '_']:
        device = get_rdevice(map_address, red)
        if device is not None:
            aircraft = device.aircraft
            registration = device.registration
            competition = device.competition

    # create igc filename and limit address part (long address in case of privacy mapping)
    igc_filename = '{}_'.format(date_.strftime('%Y-%m-%d'))
    if registration is not None:
        igc_filename += '{}.'.format(registration)
    igc_filename += '{}.igc'.format(address[:8])

    try:
        bytes_io = igc(red=red, address_=map_address, date=date_, registration=registration,
                       competition=competition, aircraft=aircraft, start=start, stop=stop)
        if bytes_io is not None:
            bytes_io.seek(0)
            # Work around, Flask send_file() with BytesIO not supported
            igc_file = FileWrapper(bytes_io)
            response = Response(igc_file)
            response.headers.set('Content-Type', 'text/plain')
            response.headers.set('Content-Disposition', 'attachment', filename=igc_filename)
            response.headers.set('Access-Control-Allow-Origin', '*')
            return response
        else:
            return '', 204
    except Exception as e:
        logging.error("live_igc error: {}".format(str(e)))
        logging.debug(traceback.format_exc())
        abort(500, 'api error')


@app.route('/api/live/fleet/<code>')
def live_fleet(code):
    response = jsonify(lfleet(red=red, code=code))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/api/live/airfield/<code>')
def live__airfield(code):
    code = code.upper()
    resp = None
    lnglats = red.geopos("GEO:L", code)
    if len(lnglats) == 1:
        resp = [round(x, 5) for x in lnglats[0][::-1]] if lnglats[0] is not None else None
    response = jsonify(resp)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app.run(debug=True)
