import logging

import click
import redis

from utils.db import Db
import setting


@click.command()
@click.argument('cmd')
@click.option('-d', '--debug', help='debug option (not delete aprs redis queues, dev purpose)', is_flag=True)
@click.option('-v', '--verbose', help='logging verbose', is_flag=True)
def main(cmd, debug, verbose):
    """
    CMD=aprs or CMD=cons
    respectively for receiver aprs task or consumer task
    """
    log_format = '%(asctime)-15s %(levelname)s: %(message)s'
    if verbose:
        logging.basicConfig(format=log_format, level=logging.DEBUG)
    else:
        logging.basicConfig(format=log_format, level=logging.INFO)

    pool = redis.ConnectionPool(host=setting.REDIS_HOST, port=setting.REDIS_PORT, db=setting.REDIS_DB,
                                decode_responses=True, encoding="utf-8")
    red = redis.Redis(connection_pool=pool)

    # check if redis is reachable
    try:
        red.ping()
    except BaseException as e:
        print("error while testing redis connection")
        print("check redis server is up and running")
        logging.debug(str(e))
        exit(1)

    db = Db(path=setting.SQLITE_PATH)

    if cmd == "aprs":
        from core.receiver import run
        run(red=red, debug=debug)

    elif cmd == "cons":
        from core.queue_ import consum_queue
        while True:
            try:
                consum_queue(red=red, db=db, delete=not debug)
            except KeyboardInterrupt:
                print("Stop consumer process")
                break
            except BaseException as e:
                logging.warning(repr(e))
    else:
        ctx = click.get_current_context()
        print(ctx.get_help())


if __name__ == "__main__":
    main()
